<!--- Submit --->
<cfif isDefined('act_form') and act_form eq 'buscar'>
</cfif>

<!--- Lectura de datos --->

<cfinvoke
	component="cursocf.componentes.paises"
	method="leer"
	returnVariable="rsPaises"
>

<cfajaxproxy cfc="cursocf.componentes.estados" jsclassname="estadosCFC">

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="../../../css/classes.css">
</head>
<body>
	
	<cfform name="formAgregar" class="ui form segment max w400 margin auto">
		<div class="field">
			<label>Pais</label>
			<cfselect name="id_pais" onchange="cargarEstados(this.value)">
				<option value="-1">Seleccione pais...</option>
				<cfoutput query="rsPaises.rs">
					<option value="#id_pais#">#nb_pais#</option>
				</cfoutput>
			</cfselect>
		</div>
		<div class="field">
			<label>Estado</label>
			<cfselect name="id_estado">
				<option value="-1">Seleccione estado...</option>
			</cfselect>
		</div>
		<div class="field">
			<label>Nombre</label>
			<cfinput type="text" name="nb_ciudad">
		</div>
		<div class="field" align="right">
			<a href="listado.cfm" class="ui button">Regresar</a>
			<button type="button" onclick="validar();" class="ui button teal">Guardar</button>
		</div>
	</cfform>

	<script type="text/javascript" src="../../../libs/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../../../libs/semantic/semantic.min.js"></script>
	<script type="text/javascript" src="../../../js/fnc.js"></script>
	<script>
		$('#id_pais, #id_estado').dropdown();

		var cargarEstados = function( idEstado ){
			var estadoCFC = new estadosCFC();
			var result = estadoCFC.leerPorPais_remoto(idEstado);
			var estados = fnc.cfJson(result.RS);
		};

		var validar = function(){
			var flag = true;
			$('.error').removeClass('error');

			if( $('#id_pais').val() == '-1' ){
				$('#id_pais').parent().parent().addClass('error');
				flag = false;
			}

			if( $('#id_estado').val() == '-1' ){
				$('#id_estado').parent().parent().addClass('error');
				flag = false;
			}

			if( $('#nb_ciudad').val() == '' ){
				$('#nb_ciudad').parent().addClass('error');
				flag = false;
			}

			//if( flag ) formAgregar.submit();
		};

		
	</script>

</body>
</html>