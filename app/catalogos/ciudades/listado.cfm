<cfparam name="filtro" default="">

<cfif isDefined('act_form') and act_form eq 'buscar'>
	<cfset args.filtro = filtro>

	<cfinvoke
		component="cursocf.componentes.ciudades"
		method="leer"
		argumentcollection="#args#"
		returnVariable="rsCiudades"
	>
</cfif>

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">

</head>
<body>
	
	<cfform name="formBuscar" class="ui form segment">
		<input type="hidden" name="act_form" value="buscar">
		<div class="two fields">
			<div class="field">
				<a href="agregar.cfm" class="ui button teal">Agregar nuevo</a>
			</div>
			<div class="field">
				<div class="ui input action">
					<cfinput type="text" name="filtro" value="#filtro#" placeholder="Buscar...">
					<button class="ui button teal">Buscar</button>
				</div>
			</div>
		</div>
	</cfform>

	<cfif isDefined('act_form') and act_form eq 'buscar'>
		<table class="ui table">
			<thead>
				<th>Pais</th>
				<th>Estado</th>
				<th>Nombre</th>
				<th></th>
			</thead>
			<tbody>
				<cfoutput query="rsCiudades.rs">
					<tr>
						<td>#nb_pais#</td>
						<td>#nb_estado#</td>
						<td>#nb_ciudad#</td>
						<td class="right aligned">
							<button class="ui mini button">Editar</button>
						</td>
					</tr>
				</cfoutput>
			</tbody>
		</table>

		<cfset args.filtro = filtro>

		<cfinvoke
			component="cursocf.componentes.ciudades"
			method="leer"
			argumentcollection="#args#"
			returnVariable="rsCiudades"
		>
	</cfif>

</body>
</html>