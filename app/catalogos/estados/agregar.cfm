<cfif isDefined('act_form') and act_form eq 'agregar'>
	<cfset args = structNew()>
	<cfset args.id_pais = id_pais>
	<cfset args.nb_estado = nb_estado>
	
	<cfinvoke
		component="cursocf.componentes.estados"
		method="guardar"
		argumentcollection="#args#"
		returnVariable="rsEstados"
	>

	<cfif rsEstados.status eq 1>
		<meta http-equiv="refresh" content="0;URL='listado.cfm'" />
	<cfelse>
		<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
		<div class="ui message error">
			<h3><cfoutput>#rsEstados.message#</cfoutput></h3>
		</div>
	</cfif>
	
	<cfabort />
</cfif>

<cfinvoke
	component="cursocf.componentes.paises"
	method="leer"
	returnVariable="rsPaises"
>

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="../../../css/classes.css">
</head>
<body>

	<div class="ui secondary pointing menu">
		<a href="../paises/listado.cfm" class="item">Paises</a>
		<a href="listado.cfm" class="item active">Estados</a>
	</div>

	<cfform name="formAgregar" class="ui form segment max w400 margin auto">
		<cfinput type="hidden" name="act_form" value="agregar">
		<div class="field">
			<label>Pais</label>
			<cfselect name="id_pais">
				<option value="-1">Seleccione pais...</option>
				<cfoutput query="rsPaises.rs">
					<option value="#id_pais#">#nb_pais#</option>
				</cfoutput>
			</cfselect>
		</div>
		<div class="field">
			<label>Nombre</label>
			<cfinput type="text" name="nb_estado">
		</div>
		<div class="field" align="right">
			<a href="listado.cfm" class="ui button">Regresar</a>
			<button type="button" onclick="validar()" class="ui button positive">Agregar</button>
		</div>
	</cfform>
	
	<script type="text/javascript" src="../../../libs/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../../../libs/semantic/semantic.min.js"></script>
	<script>
		//$('#id_pais').dropdown();

		var validar = function(){
			var flag = true;
			$('.error').removeClass('error');

			if( $('#id_pais').val() == '-1' ){
				$('#id_pais').parent().addClass('error');
				flag = false;
			}

			if( $('#nb_estado').val() == '' ){
				$('#nb_estado').parent().addClass('error');
				flag = false;
			}

			if( flag ) formAgregar.submit();
		};
	</script>
</body>
</html>