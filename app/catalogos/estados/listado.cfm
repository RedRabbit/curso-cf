<cfparam name="filtro" default="">

<cfif isDefined('act_form') and act_form eq 'buscar'>
	<cfset args = structNew()>
	<cfset args.filtro = filtro>

	<cfinvoke
		component="cursocf.componentes.estados"
		method="leer"
		argumentcollection="#args#"
		returnVariable="rsEstados"
	>
</cfif>

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
</head>
<body>
	<div class="ui secondary pointing menu">
		<a href="../paises/listado.cfm" class="item">Paises</a>
		<a href="listado.cfm" class="item active">Estados</a>
	</div>

	<cfform name="formBuscar" class="ui form segment">
		<div class="two fields">
			<div class="field">
				<a href="agregar.cfm" class="ui button icon">
					<i class="plus icon"></i>
					Agregar nuevo
				</a>
			</div>
			<div class="field">
				<div class="ui action input">
					<cfinput type="text" name="filtro" value="#filtro#" placeholder="Buscar estados...">
					<button class="ui button teal">Buscar</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="act_form" value="buscar">
	</cfform>

	<cfif isDefined('act_form') and act_form eq 'buscar'>
		<table class="ui tablet stackable table">
			<thead>
				<th>Pais</th>
				<th>Nombre</th>
				<th></th>
			</thead>
			<tbody>
				<cfoutput query="rsEstados.rs">
					<tr>
						<td>#nb_pais#</td>
						<td>#nb_estado#</td>
						<td class="right aligned">
							<form action="editar.cfm">
								<button class="ui button">Editar</button>
								<input type="hidden" name="id_estado" value="#id_estado#">
							</form>
						</td>
					</tr>
				</cfoutput>
			</tbody>
		</table>
	</cfif>
</body>
</html>