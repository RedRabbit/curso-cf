<cfif isDefined('act_form') and act_form eq 'agregar'>
	<cfset args = structNew()>
	<cfset args.id_pais = id_pais>
	<cfset args.nb_pais = nb_pais>
	
	<cfinvoke
		component="cursocf.componentes.paises"
		method="guardar"
		argumentcollection="#args#"
		returnVariable="rsPaises"
	>

	<cfif rsPaises.status eq 1>
		<meta http-equiv="refresh" content="0;URL='listado.cfm'" />
	<cfelse>
		<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
		<div class="ui message error">
			<h3><cfoutput>#rsPaises.message#</cfoutput></h3>
		</div>
	</cfif>
	
	<cfabort />
</cfif>

<cfset args = structNew()>
<cfset args.id_pais = id_pais>

<cfinvoke
	component="cursocf.componentes.paises"
	method="leer"
	argumentcollection="#args#"
	returnVariable="rsPaises"
>

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="../../../css/classes.css">
</head>
<body>

	<div class="ui secondary pointing menu">
		<a href="listado.cfm" class="item active">Piases</a>
		<a href="../estados/listado.cfm" class="item">Estados</a>
	</div>

	<cfform name="formAgregar" class="ui form segment max w400 margin auto">
		<cfinput type="hidden" name="act_form" value="agregar">
		<cfinput type="hidden" name="id_pais" value="#rsPaises.rs.id_pais#">
		
		<div class="field">
			<label>Nombre</label>
			<cfinput type="text" name="nb_pais" value="#rsPaises.rs.nb_pais#">
		</div>
		<div class="field" align="right">
			<a href="listado.cfm" class="ui button">Regresar</a>
			<button type="button" onclick="validar()" class="ui button positive">Guardar</button>
		</div>
	</cfform>
	
	<script type="text/javascript" src="../../../libs/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../../../libs/semantic/semantic.min.js"></script>
	<script>
		//$('#id_pais').dropdown();

		var validar = function(){
			var flag = true;
			$('.error').removeClass('error');

			if( $('#nb_pais').val() == '' ){
				$('#nb_pais').parent().addClass('error');
				flag = false;
			}

			if( flag ) formAgregar.submit();
		};
	</script>
</body>
</html>