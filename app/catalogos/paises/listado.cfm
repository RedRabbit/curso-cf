<!--- <cfparam name="nb_pais" default=""> --->
<cfset nb_pais = isDefined('nb_pais') ? nb_pais : ''>

<cfif isDefined('act_form') and act_form eq 'buscar'>
	<cfset args = structNew()>
	<cfset args.nb_pais = nb_pais>

	<cfinvoke
		component="cursocf.componentes.paises"
		method="leer"
		argumentcollection="#args#"
		returnVariable="rsPaises"
	>
</cfif>

<!doctype html>
<html>
<head>
	<title>Curso CF</title>
	
	<meta charset="UTF-8" />
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

	<link rel="stylesheet" type="text/css" href="../../../libs/semantic/semantic.min.css">
</head>
<body>
	<div class="ui secondary pointing menu">
		<a href="listado.cfm" class="item active">Piases</a>
		<a href="../estados/listado.cfm" class="item">Estados</a>
	</div>

	<cfform name="formBuscar" class="ui form segment">
		<div class="two fields">
			<div class="field">
				<a href="agregar.cfm" class="ui button icon">
					<i class="plus icon"></i>
					Agregar nuevo
				</a>
			</div>
			<div class="field">
				<div class="ui action input">
					<cfinput type="text" name="nb_pais" value="#nb_pais#" placeholder="Buscar paises...">
					<button class="ui button teal">Buscar</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="act_form" value="buscar">
	</cfform>

	<cfif isDefined('act_form') and act_form eq 'buscar'>
		<table class="ui tablet stackable table">
			<thead>
				<th>Nombre</th>
				<th></th>
			</thead>
			<tbody>
				<cfoutput query="rsPaises.rs">
					<tr>
						<td>#nb_pais#</td>
						<td class="right aligned">
							<form action="editar.cfm">
								<button class="ui button">Editar</button>
								<input type="hidden" name="id_pais" value="#id_pais#">
							</form>
						</td>
					</tr>
				</cfoutput>
			</tbody>
		</table>
	</cfif>
</body>
</html>