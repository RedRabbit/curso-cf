<cfcomponent>
	<cffunction name="leer" access="public" returntype="struct">
		<cfargument name="id_ciudad" type="any" required="false" default="null">
		<cfargument name="filtro" type="any" required="false" default="">

		<cftry>
			<cfquery name="query" datasource="cnx_cursoCF">
				execute sp_get_ciudades #id_ciudad#, '#filtro#'
			</cfquery>

			<cfset result.rs = query>
			<cfset result.status = 1>
			<cfset result.message = ''>

			<cfcatch>
				<cfset result.status = 0>
				<cfset result.message = cfcatch.message & ' - ' & cfcatch.detail>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<cffunction name="guardar" access="public" returntype="struct">
		<cfargument name="id_estado" type="any" required="false" default="null">
		<cfargument name="id_pais" type="any" required="true">
		<cfargument name="nb_estado" type="any" required="true">

		<cftry>
			<cfquery name="query" datasource="cnx_cursoCF">
				execute sp_save_estado #id_estado#, #id_pais#, '#nb_estado#'
			</cfquery>

			<cfset result.status = 1>
			<cfset result.message = ''>

			<cfcatch>
				<cfset result.status = 0>
				<cfset result.message = cfcatch.message & ' - ' & cfcatch.detail>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>
</cfcomponent>