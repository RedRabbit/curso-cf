var fnc = {};

fnc.cfJson = function( data ){
	var s = data || {};
	if( !s.COLUMNS && !s.DATA ){
		console.error("$scope.cfcJson() >>  was not passed a coldfusion serialized object", 'Error en JSON de ColdFusion');
		return [];
	}
	//Create returned object
	var obj = [];
	//Loops through rs and matches the columns
	for(var i=0; i < s.DATA.length; i++){
		var temp = {};
		for(var j=0; j < s.COLUMNS.length; j++){
			temp[s.COLUMNS[j].toLowerCase()] = s.DATA[i][j];
		}
		// save the new row with column names
		obj.push(temp);
	}
	// Return the objects
	return obj;
};